addin(type="equation", menu="Perform time-series cross validation", proc="tscval", desc="Takes an equation object and performs rolling out-of-sample forecasts. Returns a table object with errors averaged by horizon (e.g. "average MSE 1-period-ahead") ", docs=".\Docs\tscval.txt", url="https://raw.githubusercontent.com/jameslamb/ML4EVIEWS/master/tscval/update_info.xml") ".\tscval.prg"
addin(type="var", menu="Perform time-series cross validation", proc="tscval", desc="Takes an equation object and performs rolling out-of-sample forecasts. Returns a table object with errors averaged by horizon (e.g. "average MSE 1-period-ahead") ", docs=".\Docs\tscval.txt", url="https://raw.githubusercontent.com/jameslamb/ML4EVIEWS/master/tscval/update_info.xml") ".\tscval.prg"


